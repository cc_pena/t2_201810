package model.logic;

import java.io.*;
import java.util.ArrayList;

import com.google.gson.*;

import api.ITaxiTripsManager;
import model.vo.*;
import model.data_structures.*;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 
	public Chicago chicago;
	
	public void loadServices (String serviceFile) {
		// TODO Auto-generated method stub
		System.out.println("Inside loadServices with " + serviceFile);
		JsonParser parser = new JsonParser();

		chicago = new Chicago();
		try {
			String taxiTripsDatos = "./data/taxi-trips-wrvz-psew-subset-small.json";
			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
			JsonArray arr= (JsonArray) parser.parse(new FileReader(taxiTripsDatos));
			Company compa�ia = null;
			/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
			for (int i = 0; arr != null && i < arr.size(); i++){
				JsonObject obj= (JsonObject) arr.get(i);
				/* Mostrar un JsonObject (Servicio taxi) */
				System.out.println("------------------------------------------------------------------------------------------------");

				String company = obj.get("company") == null? "NaN": obj.get("company").getAsString();
				System.out.println(company);
				compa�ia = chicago.city.get(new Company(company));
				if(compa�ia == null){
					compa�ia = chicago.city.add(new Company(company));
				}
				/* Obtener la propiedad taxi_id de un taxi (String)*/
				String taxi_id = obj.get("taxi_id") == null? "NaN": obj.get("taxi_id").getAsString();

				Taxi taxi = compa�ia.taxiList.get(new Taxi(taxi_id));
				System.out.println("for " + i);
				if(taxi == null){
					taxi = compa�ia.taxiList.add(new Taxi(taxi_id));
					System.out.println("entra aca taxi" +i);
				}
				
				int trip_community_area = obj.get("dropoff_community_area") == null? 0: obj.get("dropoff_community_area").getAsInt();
				taxi.services.add(new Service(trip_community_area));
			}
		}
		catch (JsonIOException e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public ILinkedList<Taxi> getTaxisOfCompany(String company) {
		// TODO Auto-generated method stub
		System.out.println(chicago.city.size());
		Company compa�ia = chicago.city.get(new Company(company));
		return compa�ia.taxiList;
	}

	@Override
	public ILinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
		// TODO Auto-generated method stub
		System.out.println("Inside getTaxiServicesToCommunityArea with " + communityArea);
		MyList<Service> temp = new MyList<Service>();
		for(int i = 0; i < chicago.city.size(); i++){
			for(int j = 0; j < chicago.city.get(i).taxiList.size();j++){
				for(int j2 = 0; j2 < chicago.city.get(i).taxiList.get(j).services.size(); j2++) {
					if(communityArea == chicago.city.get(i).taxiList.get(j).services.get(j2).dropoff_community_area){
						temp.add(chicago.city.get(i).taxiList.get(j).services.get(j2));
					}
				}
			}
		}
		return temp;
	}
}