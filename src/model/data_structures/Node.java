package model.data_structures;

public class Node<T>{
	protected T element;
	protected Node<T> next;
	public Node(T pelement){
		this.element = pelement;
		next = null;
	}
	public T getElement(){
		return this.element;
	}
	public void setElement(T pelement){
		this.element = pelement;
	}
	public Node<T> getNext(){
		return next;
	}
}
