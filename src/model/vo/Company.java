package model.vo;
import model.data_structures.MyList;

public class Company implements Comparable<Company> {
	public MyList<Taxi> taxiList;
	public String name;
	public Company(String pname){
		name = pname;
		taxiList = new MyList<Taxi>();
	}
	
	@Override
	public int compareTo(Company o) {
		// TODO Auto-generated method stub
		if(this.name.equals(o.name))
			return 0;
		return 1;
	}
}
