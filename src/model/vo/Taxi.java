package model.vo;

import model.data_structures.MyList;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{
	public String taxi_id;
	public MyList<Service> services;
	public Taxi(String tax){
		taxi_id = tax;
		services = new MyList<Service>();
	}
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return "taxi Id";
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return "company";
	}
	
	@Override
	public int compareTo(Taxi o) {
		// TODO Auto-generated method stub
		if(this.taxi_id.equals(o.taxi_id))
			return 0;
		return 1;
	}	
}
