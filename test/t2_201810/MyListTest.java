package t2_201810;

import org.junit.*;
import junit.framework.TestCase;
import model.data_structures.MyList;

public class MyListTest extends TestCase
{
	/**
	 * Lista que se probar�
	 */
	private MyList<Integer> lista;

	/**
	 * Prepara el escenario de pruebas
	 */
	public void setUp()
	{
		lista = new MyList<Integer>();
		lista.add(new Integer(10));
		lista.add(new Integer(-25));
		lista.add(new Integer(13));
		lista.add(new Integer(45));
		lista.add(new Integer(-1));
		lista.add(new Integer(-8));
		lista.add(new Integer(8));
	}

	
	@Test
	public void testGet()
	{
		int a = lista.get(3);
		assertEquals("No es el numero esperado", 45, a);
		a = lista.get(6);
		assertEquals("No es el numero esperado", 8, a);
		a = lista.get(new Integer(-8));
		assertEquals("No es el numero esperado", -8, a);
		a = lista.get(new Integer(-25));
		assertEquals("No es el numero esperado", -25, a);
	}
	
	@Test
	public void testDelete(){
		MyList<Integer> temp = lista;
		int a = temp.get(1);
		boolean b = temp.delete(new Integer(-25));
		a = temp.get(1);
		assertTrue("No elimina correctamente", a == 13 && b == true);
		b = temp.delete(new Integer(0));
		assertTrue("No elimina correctamente", b == false);
	}
	
	@Test
	public void testSize(){
		MyList<Integer> temp = lista;
		assertTrue("No es el tama�o correcto", temp.size() == 7);
		temp.delete(new Integer(-25));
		assertTrue("No es el tama�o correcto", temp.size() == 6);
		lista.add(new Integer(83));
		lista.add(new Integer(134));
		assertTrue("No es el tama�o correcto", temp.size() == 8);
	}
}
